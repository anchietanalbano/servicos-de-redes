resource "aws_security_group" "servicosderedes" {
  name = "Servicos de redes"
  description = "Grupo fechado da disciplina"

  ingress {
    description      = "ssh from bastion"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    security_groups = [aws_security_group.grupo.id]
  }

  ingress {
    description = "Allow RDP from Bastion"
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    security_groups = [aws_security_group.grupo.id]
  }

  ingress {
    description = "Allow HTTP"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow All Trafic for group"
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

}

