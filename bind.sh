#!bin/bash

sudo apt-get update 
sudo apt-get upgrade -y

sudo apt install bind9 bind9utils bind9-doc -y -y

sudo sed -i 's/-u bind/-u bind -4/g' /etc/default/named

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

sudo usermod -aG root gitlab-runner 

sudo systemctl restart bind9.service

sudo gitlab-runner register \
    --non-interactive \
    --url https://gitlab.com/ \
    --token glrt-pV3BzdWBHC_DzeJqV-Ci \
    --executor shell \
