resource "aws_instance" "Bastion" {
  ami           = "ami-080e1f13689e07408"
  instance_type = "t2.micro"
  key_name = "vockey"
  
  vpc_security_group_ids = [ aws_security_group.grupo.id ]
  user_data = file("atuali.sh")
  tags = {
    Name = "Bastion host"
  }
}
