resource "aws_instance" "dns1" {
  ami           = "ami-080e1f13689e07408"
  instance_type = "t2.micro"
  key_name = "vockey"
  vpc_security_group_ids = [ aws_security_group.servicosderedes.id ]
  user_data = file("bind.sh")
  tags = {
    Name = "Dns1 host"
  }
}

/*
resource "aws_instance" "dns2" {
  ami           = "ami-080e1f13689e07408"
  instance_type = "t2.micro"
  key_name = "vockey"
  vpc_security_group_ids = [ aws_security_group.servicosderedes.id ]
  user_data = file("bind.sh")
  tags = {
    Name = "Dns2 host"
  }
}
*/